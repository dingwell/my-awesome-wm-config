---------------------------
-- Designed to work with --
-- Arch (2018-06-20)     --
-- awesome-wm v4.3.0     --
---------------------------

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget

-- Adam's customizations:
-- Widgets from streetturtle@github: awesome-wm-widgets
-- Ensure that the folder awesome-wm-widgets is located under ~/.config/awesome/

-- Create a battery widget only if there is an installed battery:
function check_for_battery()
  local battery_path="/sys/class/power_supply/BAT0"
  gears.filesystem.dir_readable(battery_path)
end

local battery_widget
if check_for_battery() then
  battery_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")
else
  battery_widget = nil
end


local volume_widget = require("awesome-wm-widgets.volume-widget.volume")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
home = os.getenv("HOME")      -- Path to home directory

wallpapertool = "nitrogen" .. " " .. home .. "/Pictures/wallpapers" -- used in menu
wallpapercmd  = "nitrogen" --restore &" -- used by my theme.lua

-- Themes define colours, icons, font and wallpapers.
beautiful.init(home .. "/.config/awesome/themes/adam/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "sakura" or "xterm"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor
filemanager = "thunar" or "nautilus"
wwwbrowser = "firefox" -- Note my rules only apply to firefox

-- Custom shutdown/logout commands:
quit_cmd = home .. "/bin/quit_awesome.sh"

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
--    awful.layout.suit.tile.left,
--    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
--    awful.layout.suit.spiral,
--    awful.layout.suit.spiral.dwindle,
--    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
--    awful.layout.suit.magnifier,
--    awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
layouts = awful.layout.layouts
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end, home .. "/.icons/Arc-Icons/apps/16/autokey.png"},
   { "manual", terminal .. " -e man awesome", home .. "/.icons/Arc-Icons/apps/16/help.png"},
   { "Set Wallpaper", wallpapertool, beautiful.menu_wallpaper_icon},
   { "Restart Conky", "restart_conky.sh", beautiful.menu_restart_icon},
   { "edit config", editor_cmd .. " " .. awesome.conffile, beautiful.menu_edit_icon},
   { "restart", awesome.restart, beautiful.menu_restart_icon},
   --{ "quit", function() awesome.quit() end}
   { "Quit", quit_cmd, beautiful.menu_quit_icon }
}

myappsmenu = {
   --{ "Claws Mail", "claws-mail", "/usr/share/pixmaps/claws-mail.png"},
   { "Firefox", "firefox", beautiful.menu_firefox_icon },
   { "Inkscape (Xephyr)","run_inkscape_under_xephyr.sh",
      "/usr/share/pixmaps/inkscape.xpm"},
   --{ "KeePass2 (USB)","external_keepass.sh", "/usr/share/pixmaps/keepass2.png"},
   { "KeePassX","keepassx", home .. "/.icons/Arc-Icons/apps/16/keepass.png"},
   --{ "Screenshot", "mate-screenshot --interactive" },
   { "Thunar Files", "thunar", beautiful.menu_files_icon },
   { "Thunderbird", "thunderbird", home .. "/.icons/Arc-Icons/apps/16/thunderbird.png"}
}

myofficemenu = { 
   { "Claws Mail", "claws-mail", "/usr/share/pixmaps/claws-mail.png"},
   { "Libre Office Calc", "libreoffice --calc", beautiful.menu_office_calc_icon},
   { "Libre Office Impress", beautiful.menu_office_impress_icon},
   { "Libre Office Write", "libreoffice --writer", beautiful.menu_office_write_icon},
   { "PDF Chain","pdfchain","/usr/share/app-install/icons/pdfchain.png"},
   { "Pybliographic", "pybliographic ~/Documents/Jobb_local/references/bibliography.bib", "/usr/share/pixmaps/pybliographic.png"},
   { "Screen Ruler","screenruler","/usr/share/pixmaps/screenruler.png"},
   { "Zim Notes", "zim", "/usr/share/pixmaps/zim.png"}
}

--mydesktopsmenu = {
--   { "XFCE-desktop","run_xfce_xephyr.sh","/usr/share/pixmaps/xfce4_xicon3.png"}
--}

mydevelopmentmenu = {
   { "Geany IDE", "geany", "/usr/share/pixmaps/geany.xpm"},
   { "gitg Git viewer", "gitg", "/usr/share/pixmaps/gtg.xpm"},
--   { "GitKraken","gitkraken",beautiful.menu_kraken_icon}
}

mysysmenu = {
  { "Disk Usage", "baobab", "/usr/share/icons/hicolor/22x22/apps/baobab.png"},
  { "Mate Control Center", "mate-control-center", "/usr/share/icons/gnome/48x48/categories/gnome-control-center.png"},
  { "Xfce Control Center", "xfce4-settings-manager", "/usr/share/icons/gnome/48x48/categories/gnome-control-center.png"},
  --{ "Time and Date", "gksu system-config-date"}
}


mymainmenu = awful.menu({ items = {
  { "awesome", myawesomemenu, beautiful.awesome_icon },
  { "Applications", myappsmenu, beautiful.menu_apps_icon },
  { "Development", mydevelopmentmenu, beautiful.menu_development_icon},
  { "Office", myofficemenu, beautiful.menu_office_icon},
  { "System Tools", mysysmenu, beautiful.menu_utilities_icon},
  { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
--menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
--mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()
--mysystray = wibox.widget.systray()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

-- Define a tag table which holds all screen tags
tags = {
  -- Used for defaults and screen 1:
  names  = { "1-W ", "2-@ ", "3-N ", "4-# ", "5-F ",
             "-6-", "7-♫ ", "-8-", "-9-" },
  layout = { layouts[2], layouts[3], layouts[2], layouts[5], layouts[5],
             layouts[2], layouts[2], layouts[2], layouts[2] },
 -- (Previously) used for screen 2:
 names2   = { "1-F", "2-#", "-3-" },
 layouts2 = { layouts[2], layouts[3], layouts[2] }
} 

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)
    
    -- Each screen has its own tag table.
    awful.tag({"1-W ", "2-@ ", "3-N ", "4-# ", "5-F ","-6-", "7-♫ ", "-8-", "-9-" },
              s, { layouts[2], layouts[3], layouts[2], layouts[5], layouts[5], 
                   layouts[2], layouts[2], layouts[2], layouts[2] } )
    --awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            --mykeyboardlayout,
            wibox.widget.systray(),
            volume_widget, --AD
            battery_widget, --AD
            mytextclock,
            s.mylayoutbox,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"}),
    -- AD: Custom key bindings:
    awful.key({ modkey, "Control" }, "l",
      function () awful.util.spawn("xtrlock") end,
      {description = "Lock screen", group = "utilities" } ),
    awful.key({ }, "#123",
      function () awful.util.spawn("amixer -D pulse sset Master 5%+") end,
      {description = "Volume up", group = "utilities"} ),
    awful.key({ }, "#122",
      function () awful.util.spawn("amixer -D pulse sset Master 5%-") end,
      {description = "Volume down", group = "utilities"} ),
    awful.key({ }, "#121",
      function () awful.util.spawn("amixer -D pulse sset Master toggle") end,
      {description = "Mute/Unmute", group = "utilities"} )
)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

--clientbuttons = gears.table.join(
clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     } },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "Dialog", -- E.g. Firefox dialogs
          "Floating window",
          "xgks",
          "freeorion",
          "Msgcompose",
        },
        class = {
          "Arandr",
          "feh",
          "Gpick",
          "Gimp",
          "Gtk-recordMyDesktop",
          "ImageMagick",
          "KeePass2",
          "Kruler",
          "keepassx",
          "MessageWin",  -- kalarm.
          "Mplayer",
          "Ncview",
          "Org.gnome.DejaDup",
          "Shutter",
          "Sozi.py",
          "Sxiv",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer",
          "Zotero",
        },
        name = {
          "Event Tester",  -- xev.
          "Screen Ruler",
          "Choose",
          "Error (pybliographer)",
          "Open",
          "Open file",
          "File Operation Progress",
          "cryptkeeper",
          "Mount stash",
          "Import File/Data",
          "Back Up",
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        },
      },
      except_any = {
        instance = { 
          "Navigator",  -- E.g. Zotero main window
        },
        name = {
          "Default - KeePassX", -- KeepassX main window (login window and others should be floating)
        },
      },
      properties = { floating = true } },

    -- Add titlebars to normal clients and dialogs
    --{ rule_any = {type = { "normal", "dialog" }
    --  }, properties = { titlebars_enabled = true }
    --},
    --
    -- Transparent clients:
    { rule_any = { 
        instance = {
          "ncview",
        },
        name = {
          "Screen Ruler",  -- Screen measuring tool
          "sakura*", -- Terminal emulator
        },
      },
      properties = { opacity = 0.8 } },

    -- Clients which should never steal focus:
    { rule_any = {
        class = {
          "xfce4-notifyd",
        },
      },
      properties = {focus = false} },
    
    -- Map specific applications to specific tags:
    { rule_any = {
        class = {
          "firefox",
        },
        instance = {
          "set_on_s1t1",
        } },
      properties = { screen = 1, tag = "1-W " } },

    { rule_any = {
        class = {
          "Claws-mail",
          "thunderbird",
          "Thunderbird"
        },
        instance = {
          "set_on_s1t2",
        } },
      properties = { screen = 1, tag = "2-@ " } },

    { rule_any = {
        name = {
          "Zim notebook - Zim",
        },
        instance = {
          "set_on_s1t3",
        } },
      properties = { screen = 1, tag = "3-N " } },

    { rule_any = {
        instance = {
          "set_on_s1t4",
        } },
      properties = { screen = 1, tag = "4-# " } },

    { rule_any = {
        name = { "Figure [0-9].*" }, -- Figures from e.g. Python, matlab ...
        class = { "Octave" } },      -- Figures from Octave
      properties = { screen = 1, tag = 5 } },
    { rule_any = { 
        name = {
          "Figure 1",
        },
        class = { 
          "matplotlib",
        },
      instance = {
          "matplotlib",
          "set_on_s1t5",
        } }, 
      properties = { screen = 1, tag = "5-F " } },

    { rule_any = {
        class = {
          "Pybliographer",
        },
        instance = {
          "set_on_s1t6",
        } },
      properties = { screen = 1, tag = "-6-" } },
      
    { rule_any = {
        class = {
          "Rhythmbox",
        },
        instance = {
          "set_on_s1t7",
        } },
      properties = { screen = 1, tag = "7-♫ " } },

    { rule_any = {
        instance = {
          "set_on_s1t8",
        } },
      properties = { screen = 1, tag = "-8-" } },

    { rule_any = {
        instance = {
          "set_on_s1t9",
        } },
      properties = { screen = 1, tag = "-9-" } },

    -- Sticky clients: shown on all tags:
    { rule = { class = "Conky" },
      properties = { sticky = true } },

}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
